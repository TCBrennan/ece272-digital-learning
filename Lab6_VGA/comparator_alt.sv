module comparator2 #(parameter N=10, M=784, O=143, P=640)
							(input logic [N-1:0] a,
							output logic altb, agtc,
							output logic [P-1:0] active_value);
	
	reg [N-1:0] b = M;
	reg [N-1:0] c = O;
	assign altb = (a < b);
	assign agtc = (a > c);
	assign active_value = (a - c);
endmodule 