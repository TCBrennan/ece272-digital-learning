module color_mux( input logic [3:0] data, gnd,
						input logic sel,
						output logic [3:0] value);

		assign value = sel ? data : gnd;
endmodule
