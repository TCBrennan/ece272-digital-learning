// Copyright (C) 2018  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details.

// PROGRAM		"Quartus Prime"
// VERSION		"Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"
// CREATED		"Thu May 06 19:33:21 2021"

module ECE272_Lab4(
	CLK,
	R,
	C_in,
	B,
	Sa,
	Sb,
	Sc,
	Sd,
	Se,
	Sf,
	Sg,
	C_out
);


input wire	CLK;
input wire	R;
input wire	C_in;
input wire	[3:0] B;
output wire	Sa;
output wire	Sb;
output wire	Sc;
output wire	Sd;
output wire	Se;
output wire	Sf;
output wire	Sg;
output wire	C_out;

reg	[3:0] A;
wire	[3:0] Z;





ECE272_lab3	b2v_inst(
	.D0(A[0]),
	.D1(A[1]),
	.D2(A[2]),
	.D3(A[3]),
	.Sa(Sa),
	.Sb(Sb),
	.Sc(Sc),
	.Sd(Sd),
	.Se(Se),
	.Sf(Sf),
	.Sg(Sg));


always@(posedge CLK or negedge R)
begin
if (!R)
	begin
	A[3] <= 0;
	end
else
	begin
	A[3] <= Z[3];
	end
end


always@(posedge CLK or negedge R)
begin
if (!R)
	begin
	A[2] <= 0;
	end
else
	begin
	A[2] <= Z[2];
	end
end


always@(posedge CLK or negedge R)
begin
if (!R)
	begin
	A[1] <= 0;
	end
else
	begin
	A[1] <= Z[1];
	end
end


always@(posedge CLK or negedge R)
begin
if (!R)
	begin
	A[0] <= 0;
	end
else
	begin
	A[0] <= Z[0];
	end
end


Full_adder_4	b2v_inst2(
	.pin_name1(C_in),
	.A(A),
	.B(B),
	.C_out(C_out),
	.Z(Z));


endmodule
