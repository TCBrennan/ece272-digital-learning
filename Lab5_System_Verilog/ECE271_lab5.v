// Copyright (C) 2018  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details.

// PROGRAM		"Quartus Prime"
// VERSION		"Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"
// CREATED		"Fri May 14 09:53:48 2021"

module ECE271_lab5(
	CLK,
	Reset,
	Seg0,
	Seg1,
	Seg2,
	Seg3,
	Seg4,
	Seg5
);


input wire	CLK;
input wire	Reset;
output wire	[6:0] Seg0;
output wire	[6:0] Seg1;
output wire	[6:0] Seg2;
output wire	[6:0] Seg3;
output wire	[6:0] Seg4;
output wire	[6:0] Seg5;

wire	[24:0] Clk_div;
wire	R;
wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_26;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_27;
wire	SYNTHESIZED_WIRE_5;
wire	[6:0] SYNTHESIZED_WIRE_28;
wire	[6:0] SYNTHESIZED_WIRE_29;
wire	[3:0] SYNTHESIZED_WIRE_30;
wire	[3:0] SYNTHESIZED_WIRE_11;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_15;
wire	SYNTHESIZED_WIRE_16;
wire	SYNTHESIZED_WIRE_17;
wire	[3:0] SYNTHESIZED_WIRE_18;
wire	[3:0] SYNTHESIZED_WIRE_20;
wire	[3:0] SYNTHESIZED_WIRE_21;
wire	[3:0] SYNTHESIZED_WIRE_22;
wire	[3:0] SYNTHESIZED_WIRE_23;
wire	SYNTHESIZED_WIRE_24;
wire	SYNTHESIZED_WIRE_25;





sync	b2v_inst(
	.clk(CLK),
	.d(SYNTHESIZED_WIRE_0),
	.q(SYNTHESIZED_WIRE_26));


NCounter	b2v_inst10(
	.clk(Clk_div[5]),
	.reset(SYNTHESIZED_WIRE_1),
	.q(SYNTHESIZED_WIRE_28));
	defparam	b2v_inst10.N = 7;


NCounter	b2v_inst11(
	.clk(SYNTHESIZED_WIRE_26),
	.reset(SYNTHESIZED_WIRE_3),
	.q(SYNTHESIZED_WIRE_29));
	defparam	b2v_inst11.N = 7;


NCounter	b2v_inst12(
	.clk(SYNTHESIZED_WIRE_27),
	.reset(SYNTHESIZED_WIRE_5),
	.q(SYNTHESIZED_WIRE_30));
	defparam	b2v_inst12.N = 4;


comparator	b2v_inst14(
	.a(SYNTHESIZED_WIRE_28),
	.altb(SYNTHESIZED_WIRE_15));
	defparam	b2v_inst14.M = 60;
	defparam	b2v_inst14.N = 7;


comparator	b2v_inst15(
	.a(SYNTHESIZED_WIRE_29),
	.altb(SYNTHESIZED_WIRE_16));
	defparam	b2v_inst15.M = 60;
	defparam	b2v_inst15.N = 7;


comparator	b2v_inst16(
	.a(SYNTHESIZED_WIRE_30),
	.altb(SYNTHESIZED_WIRE_17));
	defparam	b2v_inst16.M = 12;
	defparam	b2v_inst16.N = 4;


NCounter	b2v_inst17(
	.clk(CLK),
	.reset(R),
	.q(Clk_div));
	defparam	b2v_inst17.N = 25;


parser	b2v_inst18(
	.a(SYNTHESIZED_WIRE_28),
	.ones(SYNTHESIZED_WIRE_11),
	.tens(SYNTHESIZED_WIRE_18));
	defparam	b2v_inst18.N = 7;


parser	b2v_inst19(
	.a(SYNTHESIZED_WIRE_29),
	.ones(SYNTHESIZED_WIRE_20),
	.tens(SYNTHESIZED_WIRE_21));
	defparam	b2v_inst19.N = 7;


sevenseg	b2v_inst2(
	.data(SYNTHESIZED_WIRE_11),
	.segments(Seg0));


assign	SYNTHESIZED_WIRE_5 = SYNTHESIZED_WIRE_12 | R;

assign	SYNTHESIZED_WIRE_3 = SYNTHESIZED_WIRE_27 | R;

assign	SYNTHESIZED_WIRE_1 = SYNTHESIZED_WIRE_26 | R;

assign	SYNTHESIZED_WIRE_0 =  ~SYNTHESIZED_WIRE_15;

assign	SYNTHESIZED_WIRE_24 =  ~SYNTHESIZED_WIRE_16;

assign	SYNTHESIZED_WIRE_25 =  ~SYNTHESIZED_WIRE_17;

assign	R =  ~Reset;


sevenseg	b2v_inst3(
	.data(SYNTHESIZED_WIRE_18),
	.segments(Seg1));


parser_mod	b2v_inst30(
	.a(SYNTHESIZED_WIRE_30),
	.ones(SYNTHESIZED_WIRE_22),
	.tens(SYNTHESIZED_WIRE_23));
	defparam	b2v_inst30.N = 4;


sevenseg	b2v_inst4(
	.data(SYNTHESIZED_WIRE_20),
	.segments(Seg2));


sevenseg	b2v_inst5(
	.data(SYNTHESIZED_WIRE_21),
	.segments(Seg3));


sevenseg	b2v_inst6(
	.data(SYNTHESIZED_WIRE_22),
	.segments(Seg4));


sevenseg	b2v_inst7(
	.data(SYNTHESIZED_WIRE_23),
	.segments(Seg5));


sync	b2v_inst8(
	.clk(CLK),
	.d(SYNTHESIZED_WIRE_24),
	.q(SYNTHESIZED_WIRE_27));


sync	b2v_inst9(
	.clk(CLK),
	.d(SYNTHESIZED_WIRE_25),
	.q(SYNTHESIZED_WIRE_12));


endmodule
