module comparator #(parameter N=8, M=60)
							(input logic [N-1:0] a,
							output logic altb);
	
	reg [N-1:0] b = M;
	assign altb = (a < b);
endmodule 