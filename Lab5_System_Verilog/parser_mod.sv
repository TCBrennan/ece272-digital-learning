module parser_mod 
				(input logic [31:0] a,
			input logic enable,	
				output logic [15:0] comand, address);
		assign address = enable ? &a[15:0]: 16'bz;
		assign comand = enable ? &a[31:16]: 16'bz;
endmodule 
