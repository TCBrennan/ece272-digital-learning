module display_mux #(parameter N=12)
							(input logic [N-1:0] sprite, background,
							input logic enable,
							output logic [N-1:0] value);
	assign value = ( enable ? sprite : background);
	endmodule
	
							