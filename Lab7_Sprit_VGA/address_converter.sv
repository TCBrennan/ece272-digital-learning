module address_converter #(parameter N = 16, Width=256)
								(input logic [9:0] row, column,
									output logic [N-1:0] address);
		assign address = ((row * Width) + column);
endmodule
