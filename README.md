# README #

### ECE 272 Digital Logic Laboratory ###

This lab enables opportunities that facilitate learning both combinational and simple sequential designs. These designs are implemented using a IntelFPGA through schematic capture and Verilog. Some examples of projects are adders, 4 digit seven segment display controllers, and even VGA output.

https://eecs.oregonstate.edu/tekbots/courses/ece272

# Technology and Tools used #

* Terasic DE10-LITE Field Programable Gate Array
* Altera Quartus
* ModelSim

### Section 1: Basic Combinational Logic and the DE10-Lite ###

This section covers the basics of using the Quartus Prime software to program an FPGA, and use of the onboard switches, buttons, and LEDs. 

### Section 2: Adders on an FPGA ###

Learn about number systems and adders.

### Section 3: Combinational Logic (Seven Segment Driver) ###

Design a 7 segment display decoder.

### Section 4:Counters ###

Design a counter and display it on the 7 segment display.

### Section 5: System Verilog ###

Use System Verilog to desgin an accurate clock.

### Section 6: Video Graphics Array(VGA) ###

Design a system that uses the onboard VGA port to output basic graphics.

### Section 7: Displaying Images with VGA ###

Design a system that uses the onboard VGA port to output basic graphics.

### ECE271 Group Project ###

Using the section 7 lab, and in a group of 3, and IR decoder was developed. The purpose of this lab was to work in a group, and to properly document our project. The IR decoder was considered the Extra Credit assignment, and our final design did not end up working. But our overall grade was higher then a 100%. The IR was transmitting in RC5.

### How do I get set up? ###

* All needed blocks are provided in each section, but ModelSim code is not. Inorder to simulate, new code would need to be created. You will need to install Quartus, have a working FPGA, and program the pins accordingly.



### Who do I talk to? ###

* Repo owner or admin