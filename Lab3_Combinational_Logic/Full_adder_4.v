// Copyright (C) 2018  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details.

// PROGRAM		"Quartus Prime"
// VERSION		"Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"
// CREATED		"Thu Apr 08 19:04:41 2021"

module Full_adder_4(
	pin_name1,
	A,
	B,
	C_out,
	Z
);


input wire	pin_name1;
input wire	[3:0] A;
input wire	[3:0] B;
output wire	C_out;
output wire	[3:0] Z;

wire	[3:0] Z_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;





lab2_ece112	b2v_inst(
	.A(A[0]),
	.B(B[0]),
	.C_in(pin_name1),
	.S(Z_ALTERA_SYNTHESIZED[0]),
	.C_out(SYNTHESIZED_WIRE_0));


lab2_ece112	b2v_inst1(
	.A(A[1]),
	.B(B[1]),
	.C_in(SYNTHESIZED_WIRE_0),
	.S(Z_ALTERA_SYNTHESIZED[1]),
	.C_out(SYNTHESIZED_WIRE_1));


lab2_ece112	b2v_inst2(
	.A(A[2]),
	.B(B[2]),
	.C_in(SYNTHESIZED_WIRE_1),
	.S(Z_ALTERA_SYNTHESIZED[2]),
	.C_out(SYNTHESIZED_WIRE_2));


lab2_ece112	b2v_inst3(
	.A(A[3]),
	.B(B[3]),
	.C_in(SYNTHESIZED_WIRE_2),
	.S(Z_ALTERA_SYNTHESIZED[3]),
	.C_out(C_out));

assign	Z = Z_ALTERA_SYNTHESIZED;

endmodule
