vsim -gui work.ECE272_lab3
add wave *
force D3 0 @ 0, 1 @ 80
force D2 0 @ 0, 1 @ 40 -r 80
force D1 0 @ 0, 1 @ 20 -r 40
force D0 0 @ 0, 1 @ 10 -r 20
run 160
