// Copyright (C) 2018  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details.

// PROGRAM		"Quartus Prime"
// VERSION		"Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"
// CREATED		"Fri Apr 23 11:07:04 2021"

module ECE272_lab3(
	D0,
	D1,
	D2,
	D3,
	Sa,
	Sb,
	Sc,
	Sd,
	Se,
	Sg,
	Sf
);


input wire	D0;
input wire	D1;
input wire	D2;
input wire	D3;
output wire	Sa;
output wire	Sb;
output wire	Sc;
output wire	Sd;
output wire	Se;
output wire	Sg;
output wire	Sf;

wire	SYNTHESIZED_WIRE_59;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_60;
wire	SYNTHESIZED_WIRE_61;
wire	SYNTHESIZED_WIRE_62;
wire	SYNTHESIZED_WIRE_34;
wire	SYNTHESIZED_WIRE_35;
wire	SYNTHESIZED_WIRE_36;
wire	SYNTHESIZED_WIRE_37;
wire	SYNTHESIZED_WIRE_38;
wire	SYNTHESIZED_WIRE_39;
wire	SYNTHESIZED_WIRE_40;
wire	SYNTHESIZED_WIRE_41;
wire	SYNTHESIZED_WIRE_42;
wire	SYNTHESIZED_WIRE_43;
wire	SYNTHESIZED_WIRE_44;
wire	SYNTHESIZED_WIRE_45;
wire	SYNTHESIZED_WIRE_46;
wire	SYNTHESIZED_WIRE_47;
wire	SYNTHESIZED_WIRE_48;
wire	SYNTHESIZED_WIRE_49;
wire	SYNTHESIZED_WIRE_50;
wire	SYNTHESIZED_WIRE_53;
wire	SYNTHESIZED_WIRE_54;
wire	SYNTHESIZED_WIRE_55;
wire	SYNTHESIZED_WIRE_56;




assign	SYNTHESIZED_WIRE_60 =  ~D3;

assign	SYNTHESIZED_WIRE_61 =  ~D2;

assign	SYNTHESIZED_WIRE_5 = D2 & D1 & SYNTHESIZED_WIRE_59;

assign	SYNTHESIZED_WIRE_3 = D3 & D2 & SYNTHESIZED_WIRE_59;

assign	SYNTHESIZED_WIRE_4 = D3 & D1 & D0;

assign	Sb = SYNTHESIZED_WIRE_2 | SYNTHESIZED_WIRE_3 | SYNTHESIZED_WIRE_4 | SYNTHESIZED_WIRE_5;

assign	SYNTHESIZED_WIRE_36 = SYNTHESIZED_WIRE_60 & SYNTHESIZED_WIRE_61 & D1 & SYNTHESIZED_WIRE_59;

assign	SYNTHESIZED_WIRE_47 = SYNTHESIZED_WIRE_60 & D2 & SYNTHESIZED_WIRE_62 & SYNTHESIZED_WIRE_59;

assign	SYNTHESIZED_WIRE_50 = D3 & SYNTHESIZED_WIRE_61 & D1 & SYNTHESIZED_WIRE_59;

assign	SYNTHESIZED_WIRE_46 = D3 & D2 & SYNTHESIZED_WIRE_62 & D0;

assign	SYNTHESIZED_WIRE_41 = D3 & D2 & SYNTHESIZED_WIRE_62 & SYNTHESIZED_WIRE_59;

assign	SYNTHESIZED_WIRE_40 = SYNTHESIZED_WIRE_60 & D2 & D1 & D0;

assign	SYNTHESIZED_WIRE_62 =  ~D1;

assign	SYNTHESIZED_WIRE_34 = D3 & D2 & D1;

assign	SYNTHESIZED_WIRE_35 = D3 & D2 & SYNTHESIZED_WIRE_59;

assign	SYNTHESIZED_WIRE_48 = SYNTHESIZED_WIRE_61 & SYNTHESIZED_WIRE_62 & D0;

assign	SYNTHESIZED_WIRE_49 = D2 & D1 & D0;

assign	SYNTHESIZED_WIRE_39 = SYNTHESIZED_WIRE_60 & D2 & SYNTHESIZED_WIRE_62;

assign	SYNTHESIZED_WIRE_37 = SYNTHESIZED_WIRE_61 & SYNTHESIZED_WIRE_62 & D0;

assign	SYNTHESIZED_WIRE_43 = SYNTHESIZED_WIRE_60 & SYNTHESIZED_WIRE_61 & D0;

assign	SYNTHESIZED_WIRE_44 = SYNTHESIZED_WIRE_60 & SYNTHESIZED_WIRE_61 & D1;

assign	SYNTHESIZED_WIRE_45 = SYNTHESIZED_WIRE_60 & D1 & D0;

assign	SYNTHESIZED_WIRE_42 = SYNTHESIZED_WIRE_60 & SYNTHESIZED_WIRE_61 & SYNTHESIZED_WIRE_62;

assign	SYNTHESIZED_WIRE_59 =  ~D0;

assign	SYNTHESIZED_WIRE_38 = SYNTHESIZED_WIRE_60 & D0;

assign	Sc = SYNTHESIZED_WIRE_34 | SYNTHESIZED_WIRE_35 | SYNTHESIZED_WIRE_36;

assign	Se = SYNTHESIZED_WIRE_37 | SYNTHESIZED_WIRE_38 | SYNTHESIZED_WIRE_39;

assign	Sg = SYNTHESIZED_WIRE_40 | SYNTHESIZED_WIRE_41 | SYNTHESIZED_WIRE_42;

assign	Sf = SYNTHESIZED_WIRE_43 | SYNTHESIZED_WIRE_44 | SYNTHESIZED_WIRE_45 | SYNTHESIZED_WIRE_46;

assign	Sd = SYNTHESIZED_WIRE_47 | SYNTHESIZED_WIRE_48 | SYNTHESIZED_WIRE_49 | SYNTHESIZED_WIRE_50;

assign	SYNTHESIZED_WIRE_53 = D0 ^ D2;

assign	SYNTHESIZED_WIRE_54 = D1 ^ D2;

assign	SYNTHESIZED_WIRE_56 = SYNTHESIZED_WIRE_62 & SYNTHESIZED_WIRE_60 & SYNTHESIZED_WIRE_53;

assign	SYNTHESIZED_WIRE_55 = D0 & D3 & SYNTHESIZED_WIRE_54;

assign	Sa = SYNTHESIZED_WIRE_55 | SYNTHESIZED_WIRE_56;

assign	SYNTHESIZED_WIRE_2 = SYNTHESIZED_WIRE_60 & D2 & SYNTHESIZED_WIRE_62 & D0;


endmodule
